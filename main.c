#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "hiredis.h"

static unsigned long long getmicrosec(void)
{
	struct timeval	tv;
	
	gettimeofday(&tv, NULL);
	
	return ((unsigned long long)tv.tv_sec)*1000000 + (unsigned long long)tv.tv_usec;
}

static unsigned long long printtimediff(unsigned long long start, unsigned long long end)
{
	unsigned long long	timediff = end - start;
	
	if (1.0 < ((double)timediff)/1000000)
	{
		printf("%f[s]\n", ((double)timediff)/1000000);
		return timediff;
	}
	else if (1.0 < ((double)timediff)/1000)
	{
		printf("%f[ms]\n", ((double)timediff)/1000);
		return timediff;
	}
	else
	{
		printf("%llu[us]\n", timediff);
		return timediff;
	}
}

int main(void)
{
	unsigned int		i;
	redisContext		*c;
	redisReply			*reply;
	unsigned long long	start, end;
	
	struct timeval	timeout = { 1, 500000 }; // 1.5 seconds
	c = redisConnectWithTimeout((char*)"127.0.0.1", 6379, timeout);
	if (c == NULL || c->err) {
		if (c) {
			printf("Connection error: %s\n", c->errstr);
			redisFree(c);
		} else {
			printf("Connection error: can't allocate redis context\n");
		}
		exit(1);
	}

	/* PING server */
	reply = redisCommand(c, "PING");
	printf("PING: %s\n", reply->str);
	freeReplyObject(reply);
	
	/* Set */
	reply = redisCommand(c, "SET %s %s", "foo", "hello world");
	printf("SET: %s\n", reply->str);
	freeReplyObject(reply);
	/* Get */
	reply = redisCommand(c, "GET foo");
	printf("GET foo: %s\n", reply->str);
	freeReplyObject(reply);
	/* Delete */
	reply = redisCommand(c, "DEL foo");
	freeReplyObject(reply);
	
	/* Set a key using binary safe API */
	reply = redisCommand(c, "SET %b %b", "bar", 3, "hello", 4);
	printf("SET (binary API): %s\n", reply->str);
	freeReplyObject(reply);
	/* Get */
	reply = redisCommand(c, "GET bar");
	printf("GET foo: %s\n", reply->str);
	freeReplyObject(reply);
	/* Delete */
	reply = redisCommand(c, "DEL bar");
	freeReplyObject(reply);
	
	/* INCR */
	reply = redisCommand(c, "INCR counter");
	printf("INCR counter: %lld\n", reply->integer);
	freeReplyObject(reply);
	/* INCR again */
	reply = redisCommand(c, "INCR counter");
	printf("INCR counter: %lld\n", reply->integer);
	freeReplyObject(reply);
	/* Delete */
	reply = redisCommand(c, "DEL counter");
	freeReplyObject(reply);
	
	if (NULL == (reply = redisCommand(c, "SAVE 60 100000"))) {
		exit(1);
	}
	else
	{
		freeReplyObject(reply);
	}
	
	/* Set */
	start = getmicrosec();
	if (NULL == (reply = redisCommand(c, "MULTI"))) {
		exit(1);
	}
	else
	{
		freeReplyObject(reply);
	}
	for (i = 0; i < 65535; i++) {
		char	key[16];
		char	val[16];
		snprintf(key, 16, "key%5u", i);
		snprintf(val, 16, "val%5u", i);
		reply = redisCommand(c, "SET %s %s", key, val);
		freeReplyObject(reply);
	}
	if (NULL == (reply = redisCommand(c, "EXEC"))) {
		exit(1);
	}
	else
	{
		freeReplyObject(reply);
	}
	end = getmicrosec();
	printtimediff(start, end);
	/* Get */
	start = getmicrosec();
	for (i = 0; i < 65535; i++) {
		char	key[16];
		snprintf(key, 16, "key%5u", i);
		reply = redisCommand(c, "GET %s", key);
		//printf("GET %s: %s\n", key, reply->str);
		freeReplyObject(reply);
	}
	end = getmicrosec();
	printtimediff(start, end);
	/* Delete */
	start = getmicrosec();
	for (i = 0; i < 65535; i++) {
		char	key[16];
		snprintf(key, 16, "key%5u", i);
		reply = redisCommand(c, "DEL %s", key);
		freeReplyObject(reply);
	}
	end = getmicrosec();
	printtimediff(start, end);
	
	return 0;
}
